#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#define MAX_BUFFER 1024

int main(){

	int fd;
	char buf[MAX_BUFFER];

	mkfifo("/tmp/mififo",0666);
	  
	fd = open("/tmp/mififo", O_RDONLY);

	if (fd<0) perror("Error al abrir el pipe\n");

	while (1){

		read(fd, buf, sizeof(buf));
		printf("%s\n", buf);

	 	if (strcmp(buf, "Fin del programa") == 0) break;
	}
	
	close (fd);
	unlink("/tmp/mififo");

	return 0;
}
